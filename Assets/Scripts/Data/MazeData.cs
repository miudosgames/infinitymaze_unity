﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MazeData
{
    public List<MazeFloor> floors = new List<MazeFloor>();


    public List<MazePart> GetCrossParts(MazePart part)
    {
        List<MazePart> crossParts = new List<MazePart>()
        {
            GetSidePart(part, PartAroundType.Edge_Up),
            GetSidePart(part, PartAroundType.Edge_Right),
            GetSidePart(part, PartAroundType.Edge_Down),
            GetSidePart(part, PartAroundType.Edge_Left)
        };

        return CleanPartList(crossParts);
    }


    public List<MazePart> GetAroundParts(MazePart part)
    {
        List<MazePart> aroundParts = new List<MazePart>()
        {
            GetSidePart(part, PartAroundType.Edge_Up),
            GetSidePart(part, PartAroundType.Edge_Right),
            GetSidePart(part, PartAroundType.Edge_Down),
            GetSidePart(part, PartAroundType.Edge_Left),

            GetSidePart(part, PartAroundType.Corner_Up_Right),
            GetSidePart(part, PartAroundType.Corner_Up_Left),
            GetSidePart(part, PartAroundType.Corner_Down_Right),
            GetSidePart(part, PartAroundType.Corner_Down_Left),
        };

        return CleanPartList(aroundParts);
    }


    public List<MazePart> CleanPartList(List<MazePart> parts)
    {
        for (int i = parts.Count - 1; i >= 0; i--)
        {
            if (parts[i] == null)
            {
                parts.RemoveAt(i);
                continue;
            }

            switch (parts[i].type)
            {
                case MazePartType.Empty:
                    parts.RemoveAt(i);
                    break;

                default: break;
            }
        }

        return parts;
    }


    public MazePart GetSidePart(MazePart part, PartAroundType side)
    {
        if (part == null) return null;
        MazeFloor floor = GetFloor(part.floorId);
        if (floor == null) return null;

        int x = part.gridX;
        int y = part.gridY;

        { // Set offset
            if (side == PartAroundType.Edge_Up ||
                side == PartAroundType.Corner_Up_Right ||
                side == PartAroundType.Corner_Up_Left)
                y++;

            if (side == PartAroundType.Edge_Down ||
                side == PartAroundType.Corner_Down_Right ||
                side == PartAroundType.Corner_Down_Left)
                y--;

            if (side == PartAroundType.Edge_Right ||
                side == PartAroundType.Corner_Up_Right ||
                side == PartAroundType.Corner_Down_Right)
                x--;

            if (side == PartAroundType.Edge_Left ||
                side == PartAroundType.Corner_Up_Left ||
                side == PartAroundType.Corner_Down_Left)
                x++;
        }

        int floorIndex = GetFloorIndex(part.floorId);
        { // Fixed cordinates
            if (x < 0) x = floor.width - 1;
            else if (x > floor.width - 1) x = 0;

            if (y < 0)
            {
                floorIndex--;
                if (floorIndex < 0)
                    return null;

                y = floors[floorIndex].height - 1;
            }
            else if (y > floor.height - 1)
            {
                floorIndex++;
                if (floorIndex > floors.Count - 1)
                    return null;

                y = 0;
            }
        }

        { // Search Side Part
            MazeFloor targetFloor = floors[floorIndex];
            int partIndex = MazeConstructor.GetPartIndex(x, y, targetFloor.width);

            if (partIndex < 0 || partIndex >= targetFloor.parts.Count)
                return null;

            return targetFloor.parts[partIndex];
        }
    }


    public MazeFloor GetFloor(int id)
    {
        foreach (var f in floors)
            if (f.id == id)
                return f;

        return null;
    }


    public int GetFloorIndex(int id)
    {
        for (int i = 0, n = floors.Count; i < n; i++)
            if (floors[i].id == id)
                return i;

        return -1;
    }


    public int GetPartDistance(MazePart a, MazePart b)
    {
        int widthDistance = 0;
        { // width
            int floorWidth = floors[0].width;
            widthDistance = Mathf.Abs(a.gridX - b.gridX);
            if (widthDistance > Mathf.FloorToInt((float)floorWidth / 2f))
                widthDistance = floorWidth - widthDistance;
        }

        int heightDistance = 0;
        { // Height
            if (a.floorId == b.floorId)
                heightDistance = Mathf.Abs(a.gridY - b.gridY);
            else
            {
                int floorIndexA = GetFloorIndex(a.floorId);
                int floorIndexB = GetFloorIndex(b.floorId);

                if (floorIndexA < 0 || floorIndexB < 0) // Maze Data dont contais all points
                    return -1;

                int startIndex = 0;
                int endIndex = 0;

                if (floorIndexA < floorIndexB)
                {
                    heightDistance = (floors[floorIndexA].height - a.gridY) + (b.gridY);

                    startIndex = floorIndexA;
                    endIndex = floorIndexB;
                }
                else
                {
                    heightDistance = (floors[floorIndexB].height - b.gridY) + (a.gridY);

                    startIndex = floorIndexB;
                    endIndex = floorIndexA;
                }

                int offsetIndex = startIndex + 1;
                while (offsetIndex < endIndex)
                {
                    heightDistance += floors[offsetIndex].height;
                    offsetIndex++;
                }
            }
        }

        return widthDistance + heightDistance;
    }
}

[System.Serializable]
public class MazeFloor
{
    [Header("Grid")]
    public int id;

    [Header("Dimension")]
    public int height;
    public int width;

    [Header("Content")]
    public List<MazePart> parts;

    public List<MazeFloorSection> GetSections(int sectionCount)
    {
        List<MazeFloorSection> sections = new List<MazeFloorSection>();
        for (int i = 0; i < sectionCount; i++)
            sections.Add(new MazeFloorSection()
            { parts = new List<MazePart>() });

        if (parts == null || parts.Count == 0 || (width % sectionCount) != 0)
            return sections;

        int sectionWidth = width / sectionCount,
            x = 0, index = 0;
        
        for (int i = 0, c = parts.Count; i < c; i++)
        {
            x = i % width;
            index = Mathf.FloorToInt(x / sectionWidth);
            sections[index].parts.Add(parts[i]);
        }

        return sections;
    }
}

[System.Serializable]
public class MazeFloorSection
{
    [Header("Parts")]
    public List<MazePart> parts;
}

[System.Serializable]
public class MazePart
{
    [Header("Grid")]
    public int floorId;
    public int gridX;
    public int gridY;

    [Header("Data")]
    public MazePartType type;

    [Header("Pathfindig Data")]
    public System.Action<Pathfinding.PathfindingState> setPathState = null;
    private Pathfinding.PathfindingState pathState { get; set; }
    public Pathfinding.PathfindingState pathfindingState
    {
        get { return pathState; }
        set { pathState = value; setPathState?.Invoke(value); }
    }
}


public enum MazePartType
{
    Empty       = 0,
    Ground      = 1
}

public enum PartAroundType
{
    //Edge
    Edge_Up,
    Edge_Right,
    Edge_Down,
    Edge_Left,

    // Corner
    Corner_Up_Right,
    Corner_Up_Left,
    Corner_Down_Right,
    Corner_Down_Left,
}