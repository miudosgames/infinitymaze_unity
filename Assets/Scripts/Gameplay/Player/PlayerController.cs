﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance = null;


    void Awake()
    {
        if (Instance != null)
            Destroy(this.gameObject);
        else Instance = this;
    }

    void Update()
    {
        SetStick();
    }

    public static void SetStick()
    {
        Vector2 input = new Vector2()
        {
            x = Input.GetAxisRaw("Horizontal"),
            y = Input.GetAxisRaw("Vertical")
        };

        InputManager.SetStickInput(input.normalized);
    }
}
