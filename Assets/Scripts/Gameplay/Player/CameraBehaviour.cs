﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    [Header("Behaviour")]
    public Transform character;

    [Header("Position")]
    public Vector3 positionOffset;
    public float followSpeed;
    public float translateSpeed;

    [Header("Rotation")]
    public Vector3 lookOffset;
    public float lookSpeed;


    [Header("Controller")]
    private Vector3 targetPosition;
    private Vector3 toPosition;
    private Quaternion toRotation;
    private Quaternion lookQuaternion;
    private Vector3 lookDirection;


    void Update()
    {
        Follow();
    }


    void Follow()
    {
        if (character == null)
            return;

        { // Position
            targetPosition = Vector3.Lerp(targetPosition, character.position, followSpeed * Time.deltaTime);
            toPosition = Vector3.Lerp(toPosition, targetPosition + positionOffset, translateSpeed * Time.deltaTime);
            this.transform.position = toPosition;
            //this.transform.position = targetPosition + positionOffset;
        }

        { // Rotation
            lookDirection = (targetPosition + lookOffset) - this.transform.position;
            lookQuaternion = Quaternion.FromToRotation(Vector3.forward, lookDirection);
            toRotation = Quaternion.Lerp(toRotation, lookQuaternion, lookSpeed);
            this.transform.rotation = toRotation;

        }
    }


    void MathLookDirection()
    {
        lookDirection = (targetPosition + lookOffset) - this.transform.position;
        lookQuaternion = Quaternion.FromToRotation(Vector3.forward, lookDirection);
    }


    void SetView()
    {
        if (character == null) return;
        this.transform.position = character.position + positionOffset;
        this.transform.rotation = Quaternion.FromToRotation(Vector3.forward, (character.position + lookOffset) - this.transform.position);
    }


    void OnValidate()
    {
        if (!UnityEditor.EditorApplication.isPlaying)
            SetView();
    }
}
