﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputManager
{
    public static Vector2 stickInput;
    public static event StickEvent StickInputEvent;

    public static void SetStickInput(Vector2 input)
    {
        stickInput = input;
        StickInputEvent?.Invoke(stickInput);
    }
}

public delegate void StickEvent(Vector2 input);