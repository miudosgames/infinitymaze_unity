﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBehaviour : MonoBehaviour
{
    [Header("Component")]
    public Animator animator;

    [Header("Status")]
    public float speedMax;
    public float lerpSpeed;
    public float lertRotation;
    public float rotationOffset;

    [Header("Behaviour")]
    public bool toLerpSpeed;
    public bool toLerpRotation;
    public bool lookToDirection;

    private float speed;
    private Vector3 direction;
    private Vector3 rotation;


    public void Start()
    {
        InputManager.StickInputEvent += GetDirection;
    }

    public void Walk()
    {
        SetAnimatorFloat("Speed", speed);
        this.transform.position += direction * Time.deltaTime;
    }

    public void LookToDirection()
    {
        if (!lookToDirection || direction.magnitude <= 0) return;

        rotation.y = (Mathf.Atan2(-direction.z, direction.x) * Mathf.Rad2Deg) + rotationOffset;
        if (toLerpRotation)
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(rotation), lertRotation * Time.deltaTime);
        else this.transform.rotation = Quaternion.Euler(rotation);
    }

    public void GetDirection(Vector2 input)
    {
        Vector3 toDirection = new Vector3(input.x, 0, input.y) * speedMax;
        if (toLerpSpeed)
            direction = Vector3.Lerp(direction, toDirection, lerpSpeed * Time.deltaTime);
        else direction = toDirection;

        speed = direction.magnitude;

        Walk();
        LookToDirection();
    }

    public void OnDestroy()
    {
        InputManager.StickInputEvent -= GetDirection;
    }

    #region Animator Controller
    public void SetAnimatorFloat(string name, float value)
    {
        if (animator == null)
            return;

        animator.SetFloat(name, value);
    }

    public void SetAnimatorTrigger(string name)
    {
        if (animator == null)
            return;

        animator.SetTrigger(name);
    }
    #endregion
}
