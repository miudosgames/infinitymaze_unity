﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class MazePartBehaviour : MonoBehaviour
{
    public MazePart data;
    public MeshRenderer render;
    public Color baseColor;


    void Start()
    {
        data.setPathState = SetPathState;
        render = this.GetComponent<MeshRenderer>();

        SetPathState(data.pathfindingState);
    }


    public void SetPathState(PathfindingState state)
    {
        Color c = Color.white;
        switch (state)
        {
            case PathfindingState.Unknown: c = baseColor; break;
            case PathfindingState.Check: c = PathfindingDebugBehaviour.Instance.checkColor; break;
            case PathfindingState.Locked: c = PathfindingDebugBehaviour.Instance.lockedColor; break;
            case PathfindingState.PathArea: c = PathfindingDebugBehaviour.Instance.pathAreaColor; break;
            case PathfindingState.Path: c = PathfindingDebugBehaviour.Instance.pathColor; break;
            case PathfindingState.Edge: c = PathfindingDebugBehaviour.Instance.edgeColor; break;
            default: break;
        }

        SetColor(c);
    }


    public void SetColor(Color c)
    {
        if (render != null)
            render.material.color = c;
    }


    private void OnMouseDown()
    {
        PathfindingDebugBehaviour.Instance.SetPoint(this);
    }
    
}
