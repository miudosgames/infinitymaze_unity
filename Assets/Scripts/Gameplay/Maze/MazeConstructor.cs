﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeConstructor : MonoBehaviour
{
    [Header("Dimensions")]
    public int sectionWidth;
    public int sectionCount;
    public int floorHeight;

    [Header("Data")]
    public MazeData data;

    [Header("Test")]
    public GameObject partModel;
    public Material baseMaterial;
    public Transform mazeHolder;
    int heightOffset;
    int floorCount;

    public static MazeConstructor Instance { get; set; }


    public void Awake()
    {
        Instance = this;
    }


    public void Start()
    {
        data = new MazeData()
        {
            floors = new List<MazeFloor>()
        };
    }


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            { // Temp set mid position;
                Vector3 holdMidPosition = mazeHolder.position;
                holdMidPosition.x = -((float)sectionCount * (float)sectionWidth) / 2f;
                mazeHolder.position = holdMidPosition;
            }

            MazeFloor f = MakeMazeFloor(sectionCount, sectionWidth, floorHeight, floorCount);
            SetMazeFloorPath(f);
            ConstructMazeFloor(f);
            data.floors.Add(f);

            floorCount++;
        }
    }


    public static MazeFloor MakeMazeFloor(int sectionCount, int sectionWidth, int floorHeight, int id = 0)
    {
        int width = sectionCount * sectionWidth;

        List<MazePart> parts = new List<MazePart>();

        for (int i = 0, c = width * floorHeight; i < c; i++)
        {
            Vector2 partGrid = GetPartGrid(i, width);
            MazePart part = new MazePart()
            {
                floorId = id,
                gridX = (int) partGrid.x,
                gridY = (int)partGrid.y
            };

            parts.Add(part);
        }

        MazeFloor floor = new MazeFloor()
        {
            id = id,
            height = floorHeight,
            width = width,
            parts = parts,
        };

        return floor;
    }


    public static void SetMazeFloorPath(MazeFloor floor)
    {
        foreach (var p in floor.parts)
            //p.type = MazePartType.Ground;
            p.type = Random.Range(0, 2) == 0 ? MazePartType.Empty : MazePartType.Ground;
    }


    public void ConstructMazeFloor(MazeFloor floor)
    {

        foreach (var part in floor.parts)
        {
            GameObject o = Instantiate(partModel);
            o.transform.localPosition = new Vector3(part.gridX, 0, part.gridY + heightOffset);

            { // Set Part data
                MazePartBehaviour behaviour = o.AddComponent<MazePartBehaviour>();
                behaviour.data = part;
                UpdateMazePartType(behaviour);
            }
        }

        heightOffset += floorHeight;
    }


    public void UpdateMazePartType(MazePartBehaviour part)
    {
        Vector3 partPosition = part.transform.localPosition;
        part.transform.parent = mazeHolder;

        switch (part.data.type)
        {
            default:
            case MazePartType.Empty:
                part.baseColor = Color.black;
                partPosition.y = -1f;
                break;

            case MazePartType.Ground:
                part.baseColor = Color.white;
                partPosition.y = -0.5f;
                break;
        }

        part.transform.localPosition = partPosition;
    }


    #region Utils
    public static int GetPartIndex(int x, int y, int width)
    {
        return x + (y * width);
    }


    public static Vector2 GetPartGrid(int index, int gridWidth)
    {
        return new Vector2()
        {
            x = index % gridWidth,
            y = Mathf.FloorToInt(index/gridWidth)
        };
    }
    #endregion
}
