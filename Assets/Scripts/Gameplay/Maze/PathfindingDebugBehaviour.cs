﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindingDebugBehaviour : MonoBehaviour
{
    [Header("Configuration")]
    public DebugPathState drawState = DebugPathState.SetStart;
    public float pathfindingStepDelay = 1;

    [Header("Path Points")]
    public MazePartBehaviour startPoint;
    public MazePartBehaviour endPoint;
    Coroutine pathfindingRotine = null;

    [Header("Colors")]
    public Color startColor = Color.blue;
    public Color endColor = Color.red;
    public Color checkColor = Color.cyan;
    public Color lockedColor = Color.magenta;
    public Color pathAreaColor = new Color(1, 0.5f, 0, 1);
    public Color pathColor = Color.green;
    public Color edgeColor = Color.yellow;


    public static PathfindingDebugBehaviour Instance { get; set; }


    void Awake()
    {
        Instance = this;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            drawState = DebugPathState.SetStart;
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            drawState = DebugPathState.SetEnd;
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            drawState = DebugPathState.TogglePartType;
    }


    public void SetPoint(MazePartBehaviour part)
    {
        switch(drawState)
        {
            case DebugPathState.SetStart:
                SetStartPoint(part);
                break;

            case DebugPathState.SetEnd:
                SetEndPoint(part);
                break;

            case DebugPathState.TogglePartType:
                TogglePartType(part);
                break;
        }
    }


    public void SetStartPoint(MazePartBehaviour part)
    {
        if (startPoint == part)
            startPoint = null;
        else startPoint = part;

        UpdatePath();
    }


    public void SetEndPoint(MazePartBehaviour part)
    {
        if (endPoint == part)
            endPoint = null;
        else endPoint = part;

        UpdatePath();
    }


    public void TogglePartType(MazePartBehaviour part)
    {
        MazePartType type = part.data.type == MazePartType.Empty ? MazePartType.Ground : MazePartType.Empty;
        part.data.type = type;
        MazeConstructor.Instance.UpdateMazePartType(part);
        part.SetPathState(part.data.pathfindingState);

        UpdatePath();
    }


    void UpdatePath()
    {
        SetPath();
        SetPointsColor();
    }


    void SetPointsColor()
    {
        if (startPoint != null)
            startPoint.SetColor(startColor);

        if (endPoint != null)
            endPoint.SetColor(endColor);
    }


    public void SetPath()
    {
        if (startPoint == null || endPoint == null)
            Pathfinding.Pathfinding.ResetPathfindingState(MazeConstructor.Instance.data);
        else
        {
            //Pathfinding.Pathfinding.SearchLittlePath(startPoint.data, endPoint.data, MazeConstructor.Instance.data);
            if (pathfindingRotine != null)
                StopCoroutine(pathfindingRotine);

            pathfindingRotine = StartCoroutine(Pathfinding.Pathfinding.SearchPath(startPoint.data, endPoint.data, MazeConstructor.Instance.data, pathfindingStepDelay, SetPointsColor));
        }
    }
}

public enum DebugPathState
{
    SetStart,
    SetEnd,
    TogglePartType
}