﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pathfinding
{
    public static class Pathfinding
    {
        public static IEnumerator SearchPath(MazePart startPart, MazePart endPart, MazeData data, float delay, System.Action callback)
        {
            ResetPathfindingState(data);

            // Persistent Variables
            float t = Time.time;
            MazePart tempPart = null;
            List<MazePart> check = new List<MazePart>();

            { // Search Path
                List<MazePart> edge = new List<MazePart>() { startPart };
                bool searchPath = true;
                while (searchPath)
                {
                    tempPart = null;
                    { // Select Edge Part
                        int lessDistance = int.MaxValue;
                        foreach (var p in edge)
                        {
                            int distance = data.GetPartDistance(p, endPart) /*+ data.GetPartDistance(p, startPart)*/;
                            if (distance >= 0 && distance < lessDistance)
                            {
                                lessDistance = distance;
                                tempPart = p;
                            }
                        }

                        edge.Remove(tempPart);
                    }

                    if (tempPart == null)
                    {
                        Debug.Log("Dont search end");
                        break;
                    }

                    tempPart.pathfindingState = PathfindingState.Check;
                    check = data.GetCrossParts(tempPart);
                    int unknownCount = 0;
                    { // Validate new Edges
                        foreach (var p in check)
                        {
                            switch (p.pathfindingState)
                            {
                                case PathfindingState.Unknown:
                                    unknownCount++;
                                    p.pathfindingState = PathfindingState.Edge;
                                    edge.Add(p);

                                    yield return new WaitForSeconds(delay);
                                    break;

                                default: break;
                            }
                        }
                    }

                    { // Set Edge Part State
                        tempPart.pathfindingState = PathfindingState.PathArea;
                        { // Check End Search
                            if (tempPart == endPart)
                            {
                                Debug.Log("Complete Area Path");
                                searchPath = false;
                                break;
                            }
                            else if (unknownCount == 0)
                                tempPart.pathfindingState = PathfindingState.Locked;
                        }
                    }

                    yield return new WaitForSeconds(delay);
                }

                { // Clean Edges
                    foreach (var p in edge)
                    {
                        p.pathfindingState = PathfindingState.Unknown;
                        yield return new WaitForSeconds(delay);
                    }
                }
            }

            { // Check End Path Area
                if (tempPart != endPart)
                {
                    Debug.Log("Dont possible search path");
                    yield break;
                }
            }

            List<MazePart> path = new List<MazePart>();
            {// Re-Path
                while (true)
                {
                    tempPart.pathfindingState = PathfindingState.Path;
                    path.Add(tempPart);
                    if (tempPart == startPart)
                    {
                        Debug.Log("Complete Path");
                        break;
                    }

                    check = data.GetCrossParts(tempPart);
                    { // Get next path part
                        int minDistance = int.MaxValue;
                        foreach (var p in check)
                        {
                            switch (p.pathfindingState)
                            {
                                case PathfindingState.PathArea:
                                    int distance = data.GetPartDistance(p, startPart);
                                    if (distance < minDistance)
                                    {
                                        minDistance = distance;
                                        tempPart = p;
                                    }
                                    break;

                                default: break;
                            }
                        }
                    }

                    yield return new WaitForSeconds(delay);
                }
            }

            Debug.Log("Path Time: " + (Time.time - t));
            yield return new WaitForSeconds(1);
            callback?.Invoke();
        }

        /// A* => f(n) = g(n) + h(n);
        /// g(n) = Cost of start_Point to check_Part;
        /// h(n) = Cost of check_Point to end_Point
        public static List<MazePart> SearchLittlePath(MazePart startPart, MazePart endPart, MazeData data)
        {
            ResetPathfindingState(data);
            List<MazePart> path = new List<MazePart>();
            List<MazePart> edge = new List<MazePart>() { startPart };
            List<MazePart> check = new List<MazePart>();

            { // Search Path
                while (true)
                {
                    MazePart edgePart = null;
                    { // Select Edge Part
                        int lessDistance = int.MaxValue;
                        foreach (var p in edge)
                        {
                            int distance = A_Star_Heuristic(p,startPart,endPart,data);
                            if (distance >= 0 && distance < lessDistance)
                            {
                                lessDistance = distance;
                                edgePart = p;
                            }
                        }

                        edge.Remove(edgePart);
                    }

                    if (edgePart == null)
                    {
                        Debug.Log("Dont search end");
                        break;
                    }

                    check = data.GetCrossParts(edgePart);
                    int unknownCount = 0;
                    { // Validate new Edges
                        foreach (var p in check)
                        {
                            switch (p.pathfindingState)
                            {
                                case PathfindingState.Unknown:
                                    unknownCount++;
                                    p.pathfindingState = PathfindingState.Edge;
                                    edge.Add(p);
                                    break;

                                default: break;
                            }
                        }
                    }

                    { // Set Edge Part State
                        if (unknownCount == 0)
                        {
                            edgePart.pathfindingState = PathfindingState.Locked;
                            continue;
                        }
                        else
                        {
                            edgePart.pathfindingState = PathfindingState.PathArea;
                            path.Add(edgePart);

                            if (edgePart == endPart) // End Search
                                break;
                        }
                    }
                }
            }

            { // FilterPath

            }

            return path;
        }

        public static int A_Star_Heuristic(MazePart n, MazePart start, MazePart end, MazeData dataBase)
        {
            int g = dataBase.GetPartDistance(start, n);
            int h = dataBase.GetPartDistance(n, end);
            return g + h;
        }

        public static void ResetPathfindingState(MazeData data)
        {
            foreach (var f in data.floors)
                foreach (var p in f.parts)
                    p.pathfindingState = PathfindingState.Unknown;
        }
    }


    public enum PathfindingState
    {
        Unknown,
        Check,
        Edge,
        Locked,
        PathArea,
        Path
    }
}